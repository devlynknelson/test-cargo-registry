# How to use crates in this index

add this to the end of `~/.cargo/config.toml`

```
[registries]
test-reg = { index = "https://gitlab.com/devlynknelson/test-cargo-registry" }
```

then add a crate within the `Packages & Registries` tab into your `Cargo.toml` with the `test-reg` as the specified registry.

```
protocol = { version = "0.5.6-test", registry = "test-reg"}
```

> NOTE: Version resolution extensions may get confused. my vscode with rust-analyzer shows the crate.io versions.

# Publishing a crate file to this index

## Building the .crate file

From the project root[^1] run, run the following

```
cargo package
```

This will end up looking a lot like a build of the crate itself and if any error occur, they must be resolved before continuing.


## Uploading the .crate file

Before we start you should get a [access token](https://gitlab.com/-/profile/personal_access_tokens) with the `api` scope selected (this includes read and write permissions to the GitLab API).

With access token on clipboard run

```
curl --header "PRIVATE-TOKEN: <access token>" --upload-file <.crate file> "https://gitlab.com/api/v4/projects/38466663/packages/generic/<crate name>/<complete version text>/<crate name>-<complete version text>.crate"
```

if everything worked the output should be

```
{"message":"201 Created"}
```

> EXAMPLE:
> ```
> curl --header "PRIVATE-TOKEN: this_is_not_a_real_access_token" --upload-file protocol-0.5.6-test.crate "https://gitlab.com/api/v4/projects/38466663/packages/generic/protocol/0.5.6-test/protocol-0.5.6-test.crate"
> ```

## Making Version Metadata

> IMPORTANT: I will be using [cargo-index](https://github.com/ehuss/cargo-index) because typing all of the dependencies will cause mild frustration. If generating things is not your jam clicking [here](https://github.com/rust-lang/crates.io-index/blob/master/bo/nd/bondrewd-derive) will take you to the bondrewd-derive metadata from the crates.io index to use as an example.

My personal favorite technique for generating metadata is with like so

```
cargo index metadata --crate <path to the .crate> --index-url https://gitlab.com/devlynknelson/test-cargo-registry
```

> NOTE: Specifying the destination index url (`--index-url`) insures that the metadata can tell cargo to look at crates.io for crates not in our index.

Example output from `protocol 0.5.6-test`

```
dep =None index_url="https://gitlab.com/devlynknelson/test-cargo-registry"
dep =None index_url="https://gitlab.com/devlynknelson/test-cargo-registry"
dep =None index_url="https://gitlab.com/devlynknelson/test-cargo-registry"
dep =None index_url="https://gitlab.com/devlynknelson/test-cargo-registry"
dep =None index_url="https://gitlab.com/devlynknelson/test-cargo-registry"
dep =None index_url="https://gitlab.com/devlynknelson/test-cargo-registry"
dep =None index_url="https://gitlab.com/devlynknelson/test-cargo-registry"
dep =None index_url="https://gitlab.com/devlynknelson/test-cargo-registry"
dep =None index_url="https://gitlab.com/devlynknelson/test-cargo-registry"
dep =None index_url="https://gitlab.com/devlynknelson/test-cargo-registry"
{"name":"protocol","vers":"0.5.6-test","deps":[{"name":"aes-gcm","req":"^0.9.4","features":["std"],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"blake2s_simd","req":"^1.0.0","features":[],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"bondrewd","req":"^0.1.14","features":["derive","slice_fns"],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"buf_redux","req":"^0.8.4","features":[],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"flate2","req":"^1.0.24","features":[],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"log","req":"^0.4.17","features":[],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"rand","req":"^0.8.5","features":[],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"thiserror","req":"^1.0.32","features":[],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"anyhow","req":"~1.0","features":[],"optional":false,"default_features":true,"target":null,"kind":"dev","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"chrono","req":"~0.4","features":[],"optional":false,"default_features":true,"target":null,"kind":"dev","registry":"https://github.com/rust-lang/crates.io-index","package":null}],"features":{},"cksum":"fd358948a89c6b2bc156801175473e90a8e11a1b3be3cc711bc78ae7ce789805","yanked":false,"links":null}
```

The actual metadata here starts with `{` and ends with `}`. We can ignore all the other lines.

So our actual metadata will look like

```
{"name":"protocol","vers":"0.5.6-test","deps":[{"name":"aes-gcm","req":"^0.9.4","features":["std"],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"blake2s_simd","req":"^1.0.0","features":[],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"bondrewd","req":"^0.1.14","features":["derive","slice_fns"],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"buf_redux","req":"^0.8.4","features":[],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"flate2","req":"^1.0.24","features":[],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"log","req":"^0.4.17","features":[],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"rand","req":"^0.8.5","features":[],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"thiserror","req":"^1.0.32","features":[],"optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"anyhow","req":"~1.0","features":[],"optional":false,"default_features":true,"target":null,"kind":"dev","registry":"https://github.com/rust-lang/crates.io-index","package":null},{"name":"chrono","req":"~0.4","features":[],"optional":false,"default_features":true,"target":null,"kind":"dev","registry":"https://github.com/rust-lang/crates.io-index","package":null}],"features":{},"cksum":"fd358948a89c6b2bc156801175473e90a8e11a1b3be3cc711bc78ae7ce789805","yanked":false,"links":null}
```

> NOTE: saving this somewhere may be helpful.

## Creating Crate Metadata in the index

> If you are updating a crate and already know where the metadata file is located you can skip to [Adding Version Metadata to Crate Metadata](#adding-version-metadata-to-crate-metadata).

To add a new crate, first we need to create a crate metadata file which describes all published versions of the crate in a folder that cargo knows to look in.

### Find or Create Metadata Folder

> IMPORTANT: If we need cd into a folder and it does not exist please create it.

If the crate name is only 1 or 2 characters than we need to cd into folder named the amount of characters the crates name is.

> EXAMPLE: If the crate is named `aa`, our path from the root of the Index Repo would be `./2/`.

If the crate name is 3 characters then cd into folder named `3` then cd into a folder with only the first letter.

> EXAMPLE: If the crate is named `log`, our path from the root of the Index Repo would be `3/l/`.

Otherwise cd into a folder named with the first 2 characters of the crate name and then cd into a folder named with only the 3rd and 4th characters in the crate name.
> EXAMPLE: If the crate is named `log`, our path from the root of the Index Repo would be `pr/ot/`.

### Find or Create Metadata File

Now we just need to open a file with the exact same name as the crate with no extension.

> EXAMPLES:
> - crate named `aa` would need a file at `{Index Root}/2/aa`.
> - crate named `log` would need a file at `{Index Root}/3/l/log`.
> - crate named `protocol` would need a file at `{Index Root}/pr/ot/protocol`.

> For more documentation on the file structure of the index look [here](https://doc.rust-lang.org/cargo/reference/registries.html#index-format)

## Adding Version Metadata to Crate Metadata

With our metadata file open we should paste our new version metadata into a newline at the end of the file. After we save we are ready to push our updates to the index repo and use the newly uploaded crate.

[^1]: the folder containing crate's `Cargo.toml`